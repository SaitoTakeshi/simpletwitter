package chapter6.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;

@WebFilter(filterName = "LoginFilter", urlPatterns = { "/edit", "/setting" })
public class LoginFilter implements Filter {
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest)request;
		HttpServletResponse httpResponse = (HttpServletResponse)response;
		HttpSession session = httpRequest.getSession(false);

		User loginUser = (User) session.getAttribute("loginUser");
		if(loginUser != null) {
			chain.doFilter(request, response);
		} else {
			session.setAttribute("errorMessages", "ログインしてください");
			httpResponse.sendRedirect("./login");
			return;
		}
	}
	@Override
	public void init(FilterConfig config) throws ServletException {

	}
	@Override
	public void destroy() {
	}
}
