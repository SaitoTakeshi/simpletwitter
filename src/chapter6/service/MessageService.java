package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(Message message) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().update(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(Message message) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userId, String startDate, String endDate) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();
			Integer id = null;
			if(!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}

			Date initStartDate = java.sql.Date.valueOf("2020-01-01");
			if(StringUtils.isEmpty(startDate)) {
				startDate = initStartDate + " 00:00:00";
			} else {
				startDate += " 00:00:00";
			}

			Date currentDate = new Date();
			if(StringUtils.isEmpty(endDate)) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				endDate = format.format(currentDate);
			} else {
				endDate += " 23:59:59";
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM , startDate, endDate);
			commit(connection);
			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public UserMessage selectMessage(Integer selectedMessageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			Integer selectedId = null;
			if(selectedMessageId != null) {
				selectedId = selectedMessageId;
			}
			UserMessage selectedMessage = new UserMessageDao().selectMessage(connection, selectedId);
			commit(connection);
			return selectedMessage;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}