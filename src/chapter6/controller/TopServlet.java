package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	//top.jsp
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		//if logined => show tweet form
		boolean isShowMessageForm = false;
		User user = (User) request.getSession().getAttribute("loginUser");
		if (user != null) {
			isShowMessageForm = true;
		}

		String userId = request.getParameter("user_id");
		String messageId = request.getParameter("message_id");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		List<UserMessage> messages = new MessageService().select(userId, startDate, endDate);
		List<UserComment> comments = new CommentService().select(messageId);

		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);

		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}


}
