package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.Message;
import chapter6.service.MessageService;


@WebServlet("/deleteMessage")
public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	String deleteMessageId = request.getParameter("deleteMessageId");
		int messageId = Integer.parseInt(deleteMessageId);

		Message deleteMessage = new Message();
		deleteMessage.setId(messageId);

    	new MessageService().delete(deleteMessage);

    	response.sendRedirect("./");
	}
}
