package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = {"/edit"})
public class EditServlet extends HttpServlet {
	//Get parameter of selected message from top.jsp to edit.jsp
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
    	HttpSession session = request.getSession();
    	List<String> errorMessages = new ArrayList<String>();
    	String messageId = request.getParameter("selectedMessageId");

    	if(!isMessageValid(messageId,errorMessages)) {
    		session.setAttribute("errorMessages", errorMessages);
        	response.sendRedirect("./");
			return;
    	}

    	Message message = getMessage(request);
    	UserMessage selectedMessage = new MessageService().selectMessage(message.getId());
    	request.setAttribute("selectedMessage", selectedMessage);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

    //Use parameter got from top.jsp and do edit
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {
    	List<String> errorMessages = new ArrayList<String>();
    	Message updateMessage = updateMessage(request);

    	if (!isTextValid(updateMessage, errorMessages)) {
        	UserMessage selectedMessage = new MessageService().selectMessage(updateMessage.getId());
    		request.setAttribute("errorMessages", errorMessages);
    		request.setAttribute("selectedMessage", selectedMessage);

    		request.getRequestDispatcher("edit.jsp").forward(request, response);
    		return;
    	}
    	new MessageService().update(updateMessage);
    	response.sendRedirect("./");
    }

    //Get selected message method
    private Message getMessage(HttpServletRequest request) throws IOException, ServletException {
    	Message message = new Message();

    	message.setId(Integer.parseInt(request.getParameter("selectedMessageId")));
    	message.setText(request.getParameter("selectedMessage.text"));
    	return message;
    }

    //Update selected message method
    private Message updateMessage(HttpServletRequest request) throws IOException, ServletException {
    	Message message = new Message();

    	message.setId(Integer.parseInt(request.getParameter("selectedMessageId")));
    	message.setText(request.getParameter("text"));
    	return message;
    }

    //Validation for message
    private boolean isTextValid(Message message, List<String> errorMessages) {
    	String text = message.getText();

		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

    //Validation for message id
    private boolean isMessageValid(String messageId, List<String> errorMessages) {
    	if(!messageId.matches("^[0-9]*$") || StringUtils.isBlank(messageId)) {
    		errorMessages.add("不正なパラメータが入力されました");
    		return false;
    	}
    	UserMessage selectedMessage = new MessageService().selectMessage(Integer.parseInt(messageId));
    	if(selectedMessage == null) {
    		errorMessages.add("不正なパラメータが入力されました");
    		return false;
    	}
    	if (errorMessages.size() != 0) {
			return false;
    	}
    	return true;

    }


}