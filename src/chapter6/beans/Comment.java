package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

//tweet message
public class Comment implements Serializable {

	//need id,userId,text,createdDate,updatedDate
	private int id;
	private int userId;
	private int messageId;
	private String text;
	private Date updated_at;

	//getter and setter => id,userId,text,createdDate,updateDate
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}


	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

}
