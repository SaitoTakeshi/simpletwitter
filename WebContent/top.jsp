<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<title>簡易Twitter</title>
	</head>
	<body>
	<!-- header  -->
    	<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a id="logout" href="logout">ログアウト</a>
			</c:if>
		</div>
		<div class="DateSearch">
			<form>
				<input type="date" id="start" name="startDate" min="2020-01-01" value='${startDate}' ></input>
				<input type="date" id="end" name="endDate" min="2020-01-01" value='${endDate}' ></input>
				<input type="submit" value="絞り込み">
			</form>
		</div>

		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
				<div class="account">@<c:out value="${loginUser.account}" /></div>
				<div class="description"><c:out value="${loginUser.description}" /></div>
			</div>
		</c:if>
	<!-- error message  -->
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
	<!-- tweet form  -->
		<div class="form-area">
			<c:if test="${ isShowMessageForm }">
				<form id="message" action="message" method="post">
					いま、どうしてる？<br />
					<textarea name="text" cols="100" rows="5" class="tweet-box" wrap="hard"></textarea>
					<br />
					<input type="submit" value="つぶやく">（140文字まで）
				</form>
			</c:if>
		</div>
	<!-- display message  -->
		<div class="messages">
		<c:forEach items="${messages}" var="message">
			<div class="message">
			<input name="messageId" value="${message.id}" id= "id" type="hidden"/>
				<div class="account-name">
					<span class="account">
						<a href="./?user_id=<c:out value="${message.userId}"/> ">
							<c:out value="${message.account}" />
						</a>
					</span>
					<span class="name">
						<c:out value="${message.name}" />
					</span>
				</div>
				<div class="message_text">
					<c:forEach var="message_text" items="${fn:split(message.text,'
') }">
						<c:out value="${message_text}" /><br>
					</c:forEach>
				</div>
				<div class="date">
					<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
				</div>
	<!-- button for edit or delete  -->
				<div class="edit_message">
					<c:if test="${message.account == loginUser.account}">
						<div class="button" style="display:inline-flex">
							<form id="edit" action="edit" method="get">
								<input name="selectedMessageId" value="${message.id}" id= "id" type="hidden"/>
								<input type="submit" value="編集">
							</form>
							<form id ="deleteMessage" action="deleteMessage" method="get">
								<input name="deleteMessageId" value="${message.id}" id= "id" type="hidden"/>
								<input type="submit"  value="削除">
							</form>
						</div>
					</c:if>
					<br />
	<!-- display comment  -->
					<div class="comments">
						<c:forEach items="${comments}" var="comment">
						<c:if test="${comment.messageId == message.id}">
							<div class="comment">
								<div class="account-name">
									<span class="account">
										<a href="./?user_id=<c:out value="${comment.userId}"/> ">
											<c:out value="${comment.account}" />
										</a>
									</span>
									<span class="name">
										<c:out value="${comment.name}" />
									</span>
								</div>
								<div class="comment_text">
									<c:forEach var="comment_text" items="${fn:split(comment.text,'
') }">
										<c:out value="${comment_text}" /><br>
									</c:forEach>

								</div>
								<div class="date">
									<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
								<br />
							</div>
						</c:if>
						</c:forEach>
	<!-- comment form  -->
						<c:if test="${ not empty loginUser }">
						<form action="comment" method="post">
							返信<br />
							<input name="commentMessageId" value="${message.id}" id= "id" type="hidden"/>
							<textarea name="text" cols="100" rows="5" class="tweet-box" wrap="hard"></textarea>
							<br />
							<input type="submit" value="返信">（140文字まで）
						</form>
						</c:if>
					</div>
				</div>
				<br />
			</div>
		</c:forEach>
		</div>

		<div class="copyright">Copyright(c)Takeshi Saito</div>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="setting.js"></script>
	</body>
</html>